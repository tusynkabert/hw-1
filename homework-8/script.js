//1.
//DOM представляє структуру сторінки в браузері як дерево об'єктів.
//дозволяє взаємодіяти зі структурою сторінки. здіййснювати різноманітні операції.
//2.
//innerHTML дозволяє отримати або встановити HTML код вмісту елемента. 
//за допомогою innerHTML, можна вставляти HTML-елементи, теги, класи,
// стилі всередині елемента.
//innerText-ігнорує HTML код всередині елемента, вставляє "чистий текст".
//3.
//document.getElementById();
//document.querySelector();
//document.querySelectorAll();
//document.getElementsByTagName();
//document.getElementsByClassName()
//document.getElementsByName()

// в залежності від умов задачі. 
// мені зручно використовувати:
//document.querySelector(); 
//document.querySelectorAll();
//document.getElementById()



// Знайти всі параграфи на сторінці та встановити колір фону #ff0000
const listParagraf = document.querySelectorAll('p')

listParagraf.forEach(
    function (el) {
        el.style.backgroundColor = '#ff0000';
    }
)


// Знайти елемент із id="optionsList". Вивести у консоль. Знайти батьківський елемент та вивести в консоль.
//Знайти дочірні ноди, якщо вони є, і вивести в консоль назви та тип нод.
const optionsList = document.getElementById("optionsList");
console.log("optionsList: ", optionsList);
console.log("optionsList.parentElement: ", optionsList.parentElement);

optionsList.childNodes.forEach(
    (node) => {
        console.log(node.nodeName, node.nodeType);
    }
)



// Встановіть в якості контента елемента з класом testParagraph наступний параграф - This is a paragraph
const testParagraph = document.querySelector('#testParagraph');
// testParagraph.insertAdjacentHTML('afterend', '<p>This is a paragraph</p>');
testParagraph.innerHTML = '<p>This is a paragraph</p>';


// Отримати елементи, вкладені в елемент із класом main-header і вивести їх у консоль. Кожному з елементів присвоїти новий клас nav-item.
const mainHeader = document.querySelector('.main-header');
console.log('mainHeader.children', mainHeader.children);

// Робимо цикл по вкладених елеметах
for (let i = 0; i < mainHeader.children.length; i++) {

    // З масива дочірніх елеметів відбираємо конретний елемент по ключу
    const element = mainHeader.children[i];

    // До вибраного елементу добавляємо клас
    element.classList.add('nav-item')
}



// Знайти всі елементи із класом section-title. Видалити цей клас у цих елементів.
const sectionTitleList = document.querySelectorAll('.section-title');
sectionTitleList.forEach(
    //викликаємо анонімну функцію
    function (el) {
        el.classList.remove('section-title');
    }
);


