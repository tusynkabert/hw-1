/**
 * - відкриваємо параграф відносно кнопки по якій клікнув користувач
 * - при кліку на кнопку вкладки відбираємо його data attribute
 * - робити активним той елемент, який вібрали 
 * - забрати у всіх інших (неактивних) .active
 */

const tabs = document.querySelectorAll('[data-tab]');
const tabsLi = document.querySelectorAll('.tabs-content li');

tabs.forEach(function (element) {
    element.onclick = function (event) {

        // Видаляємо всім елементам клас активності
        tabsLi.forEach(
            function (li) {
                li.classList.remove('active');
            }
        );

        // Видаляємо всім кнопкам(клікам) клас активності
        tabs.forEach(
            function (tabBtn) {
                tabBtn.classList.remove('active');
            }
        );

        // відбираємо елемент по якому був клік
        const clickElement = this;

        // відбираємо data attribute цього element
        const tab = clickElement.dataset.tab;

        // відбираємо елемент(вкладку) який потрібно показати
        const tabEl = document.querySelector(tab);

        // додаємо  елементам клас активності
        tabEl.classList.add('active');

        // показуємо контент(параграф) активної вкладки
        clickElement.classList.add('active');
    }
});
