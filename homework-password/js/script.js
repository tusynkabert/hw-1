// Переключаємо видимість пароля
function togglePassword() {
  // Дістаємо з нашого HTML поле з паролем
  const inputPassword = document.querySelector(".js-input-password");

  // з новим кліком повертаємось до Password;
  if (inputPassword.type == "password") {
    inputPassword.type = "text";
  } else {
    inputPassword.type = "password";
  }
}

// перевірка пароля на правильність
function checkPassword() {
  const list = document.querySelector(".js-password-errors"); // Витягуємо конкретний список помилок
  const inputPassword = document.querySelector(".js-input-password"); // Витягуємо поле вводу пароля
  const valuePass = inputPassword.value; // Витягуємо значення пароля

  list.innerHTML = ""; // Очищуємо список помилок перед перевіркою

  // Перевірка на велику літеру
  list.innerHTML += /[A-Z]/.test(valuePass)
    ? "<li class='success'>* Велика літера</li>"
    : "<li class='error'>* Велика літера</li>";

  // Перевірка на цифри
  list.innerHTML += /\d/.test(valuePass)
    ? "<li class='success'>* Використовувати цифри</li>"
    : "<li class='error'>* Використовувати цифри</li>";

  // Перевірка на довжину пароля
  list.innerHTML +=
    valuePass.length >= 8
      ? "<li class='success'>* Не менше 8 символів</li>"
      : "<li class='error'>* Не менше 8 символів</li>";

  // Перевірка на спеціальні символи в паролі
  list.innerHTML += /[!@#$%^&*(),.?":{}|<>]/.test(valuePass)
    ? "<li class='error'>* Використовувати спеціальні символи</li>"
    : "<li class='success'>* Не використовувати спеціальні символи</li>";
}
