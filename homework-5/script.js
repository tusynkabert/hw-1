//1. це дії, які можна виконувати над об"єктами.
//використовуються для маніпуляції значення властивостей об"єкта всередині об"єкта,
//щоб не засмічувати глобальну область видимості.
//2.значення властивості об'єкта може мати будь-який тип даних:
//рядки, цифри, булеві значення, також null та undefined, масиви, функції тощо.
//3.це означає, що створюється посилання на об"єкт в змінній або функції,
// а не копія об"єкта.

function createNewUser() {
  const firstName = prompt("Введіть ім'я:");
  const lastName = prompt("Введіть прізвище:");

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    getLogin: function () {
      const loginFirstName = this.firstName.charAt(0).toLowerCase();
      const loginLastName = this.lastName.toLowerCase();
      return loginFirstName + loginLastName;
    },
    setFirstName: function (name) {
      this.firstName = name;
    },
    setLastName: function (name) {
      this.lastName = name;
    },
  };

  return newUser;
}

const user = createNewUser();
const login = user.getLogin();
console.log(login);
