document.addEventListener('DOMContentLoaded', () => {
    const tabBtns = document.querySelectorAll('.services .tabs__btn');
    const tabItems = document.querySelectorAll('.services .tabs__item');
    const tabBtnsWorks = document.querySelectorAll('.works .tabs__btn');


    // Додаємо обробник події для кожної кнопки вкладки
    tabBtns.forEach((btn) => {

        btn.addEventListener('click', (event) => {
            event.preventDefault(); // заборонити стандартний html, (щоб не скакало)

            // Відключаємо всі активні класи у кнопок і вкладок
            tabBtns.forEach(tabBtn => tabBtn.classList.remove('active'));
            tabItems.forEach(tabItem => tabItem.classList.remove('show'));

            // Додаємо активний клас для вибраної кнопки та відповідної вкладки
            btn.classList.add('active');

            // Відбираємо дата атрибут з селектором елементу який треба показати
            const tabShowEl = event.target.dataset.tabBtn;

            //показуємо активну вкладку
            document.querySelector(tabShowEl).classList.add('show');
        });
    });

    // Додаємо обробник події для кожної кнопки вкладки
    tabBtnsWorks.forEach((btn) => {

        btn.addEventListener('click', (event) => {

            // всі елементи портфоліо
            const worksImages = document.querySelectorAll('.works__gallary-item');

            const tab = event.target.getAttribute('data-tab');

            worksImages.forEach((img) => {
                const cat = img.getAttribute('data-cat')

                if (tab === 'all') {
                    img.classList.remove('hide');
                    return;
                }
                const attrArray = cat.split(' ');
                if (attrArray.includes(tab)) {
                    img.classList.remove('hide')
                } else {
                    img.classList.add('hide')
                }

            })

            // заборонити стандартний html, (щоб не скакало)
            event.preventDefault();

            // Відключаємо всі активні класи у кнопок і вкладок
            tabBtnsWorks.forEach(tabBtn => tabBtn.classList.remove('active'));

            // Додаємо активний клас для вибраної кнопки та відповідної вкладки
            btn.classList.add('active');

        });
    });

    const worksLoadMore = document.getElementById('works-load-more');

    worksLoadMore.addEventListener('click', (event) => {

        const gallery = document.querySelector('.works__gallary');

        // всі елементи портфоліо
        const worksImages = document.querySelectorAll('.works__gallary-item');

        worksImages.forEach((img) => {
            const el = img.cloneNode(true).outerHTML;
            gallery.insertAdjacentHTML('beforeend', el)
        })

        event.target.classList.add('hide')
    })



    $(".about-slider").slick({
        infinite: true,
        speed: 600,
        adaptiveHeight: true,
        arrows: false,
        fade: true,
        asNavFor: '.about-slider-nav'
    });
    $(".about-slider-nav").slick({
        asNavFor: '.about-slider',
        infinite: true,
        slidesToShow: 4,
        slidesToScroll: 1,
        dots: false,
        speed: 600,
        arrows: true,
        centerMode: true,
        focusOnSelect: true,
        prevArrow: '<span class="slick-prev slick-arrow"><svg class="svg-icon icon-left"><use xlink:href="#icon-left"></use></svg></span>',
        nextArrow: '<span class="slick-next slick-arrow"><svg class="svg-icon icon-right"><use xlink:href="#icon-right"></use></svg></span>',
    });


    var $grid = $('.masonry__grid').masonry({
        itemSelector: '.masonry__grid-item',
        columnWidth: 15,
    });



    const zoomImage = document.querySelector('.zoomImage')

    zoomImage.addEventListener('click', e => {
        if (e.target.classList.contains('zoomImage')) {
            e.target.classList.add('hide')
        } else {
            e.target.parentElement.classList.add('hide')
        }
    })

    document.querySelector('.masonry__grid').addEventListener('click', (e) => {


        if (e.target.classList.contains('zoom')) {
            const image = e.target.closest('.masonry__grid-item').querySelector('img')
            const imageUrl = image.src

            zoomImage.querySelector('img').setAttribute('src', imageUrl)
            zoomImage.classList.remove('hide')
        }

    })



    const masonryLoadBtn = document.querySelector('#mansory-load-more');
    const masonryLoader = document.querySelector('#mansory-loader');

    masonryLoadBtn.addEventListener('click', async () => {
        masonryLoadBtn.classList.add('hide');
        masonryLoader.classList.remove('hide');

        const gallery = document.querySelector('.masonry__grid');
        const masonryImages = gallery.querySelectorAll('.masonry__grid-item');


        await new Promise(resolve => setTimeout(resolve, 2000));

        masonryLoader.classList.add('hide');

        masonryImages.forEach((img) => {
            const el = img.cloneNode(true).outerHTML;
            gallery.insertAdjacentHTML('beforeend', el);
        });

        await new Promise(resolve => setTimeout(resolve, 200));

        $('.masonry__grid').masonry('reloadItems');

        $('.masonry__grid').masonry('layout');

        masonryLoadBtn.classList.remove('hide');
    });
})




