// Знаходимо елементи на сторінці за їх ідентифікаторами
const password = document.querySelector("#password"); // Поле вводу пароля
const passwordConfirm = document.querySelector("#passwordConfirm"); // Поле підтвердження пароля

const btn = document.querySelector(".btn"); // Кнопка "Підтвердити"
const errorText = document.querySelector("#error"); // Елемент для відображення помилки

// Об'єкт, який містить класи для іконок залежно від типу
const iconsClassByType = {
    show: 'fa-eye',         // Клас для відображення пароля
    hide: 'fa-eye-slash'    // Клас для сховання пароля
};

// Функція порівняння двох значень
const compare = (val1, val2) => val1 === val2;

// Функція для додавання слухача подій на іконку "Показати пароль"
const addListener = (el) => {
    el.addEventListener("click", (e) => {
        const input = el.previousElementSibling; // Отримуємо елемент попереднього сусіда (поле вводу)

        const isHidden = input.getAttribute('type') === 'password'; // Перевіряємо, чи пароль зараз схований
        if (isHidden) {
            input.setAttribute('type', 'text'); // Показати пароль
            el.classList.add(iconsClassByType.show); // Змінюємо клас іконки на показ пароля
            el.classList.remove(iconsClassByType.hide); // Видаляємо клас іконки для сховання
        } else {
            input.setAttribute('type', 'password'); // Сховати пароль
            el.classList.add(iconsClassByType.hide); // Змінюємо клас іконки на сховання пароля
            el.classList.remove(iconsClassByType.show); // Видаляємо клас іконки для показу
        }
    });
}

// Додати слухачі подій на іконки "Показати пароль"
addListener(password);
addListener(passwordConfirm);

// Додати слухача події на кнопку "Підтвердити"
btn.addEventListener('click', (e) => {
    e.preventDefault(); // Заборонити перезавантаження сторінки

    const password = document.querySelector('#input1-password');
    const passwordConfirm = document.querySelector('#input2-password');

    // Порівняти значення введених паролів
    const isSame = compare(password.value, passwordConfirm.value);

    // Відобразити відповідний результат
    if (isSame) {
        errorText.style.display = 'none'; // Сховати текст помилки
        alert('You are welcome'); // Показати повідомлення про вітання
    } else {
        errorText.style.display = 'block'; // Показати текст помилки
    }
});
