//1. для спрпощення коду. тобто,функція дозволяє
//розбивати складні завдання на більш дрібні та прості.
//після оголошення фукції, її можна викликати багато раз.
// за допомогою функції можна внести зміни в одному місці,
// щоб вони працювали всюди.

//2.для отримання результату функції (для її виконання).

//3.оператор return завершує виконання поточної функції та повертає її значення.
//оператор return заставляє функцію припинити виконання,
//якщо у функції оператор return не використовується, тоді повертається underfined.

let num1, num2;
askNumbers();

let sign = prompt("Enter '+', '-', '*' or '/'");

function askNumbers() {
  num1 = +prompt("Enter first number");
  num2 = +prompt("Enter second number");
}

const valid = () => Number.isFinite(+num1) && Number.isFinite(+num2);

const doMath = (number1, number2, sign) => {
  if (valid()) {
    switch (sign) {
      case "+":
        console.log(number1 + number2);
        return;
      case "-":
        console.log(number1 - number2);
        return;
      case "*":
        console.log(number1 * number2);
        return;
      case "/":
        console.log(number1 / number2);
        return;

      default:
        sign = prompt("Enter '+', '-', '*' or '/'");
        doMath(num1, num2, sign);
        break;
    }
  } else {
    askNumbers();
    doMath(num1, num2, sign);
  }
};

doMath(num1, num2, sign);
