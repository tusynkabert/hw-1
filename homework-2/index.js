//1. 8 типів даних:
//-number для будь-яких чисел.
//-bigint для цілих чисел довільної довжини.
//-string для рядків.
//-boolean для true або false.
//-null для невідомих значень — автономний тип, який має єдине значення null.
//-undefined для неприсвоєних значень — автономний тип, який має єдине значення undefined.
//-object для більш складних структур даних.
//-symbol для унікальних ідентифікаторів.
//2. у чому різниця між == та ===?
//- в відміну від не строгого порівняння == яке порівнює дані,
// строге порівняння === порівнює також типи даних.
//3. Що таке оператор?
// оператор - це інструкція для мови програмування js.
// оператором ми описуємо дії, які необхідно виконати.

let name = "";
let age = "";

askName();
askAge();

function validationAge() {
  if (age < 18 && age !== null && age !== "") {
    alert("You are not allowed to visit this website");
  } else if (age >= 18 && age <= 22) {
    const confirmAccess = confirm("Are you sure you want to continue?");

    if (confirmAccess) {
      validationName();
    } else {
      alert("You are not allowed to visit this website");
    }
  } else if (age > 22) {
    validationName();
  } else {
    askAge();
  }
}

function validationName() {
  if (name) {
    alert("Welcome " + name);
  }

  while (!name) {
    const namePrompt = askName();
    if (namePrompt) {
      alert("Welcome " + name);
      break;
    }
  }
}

function askName() {
  const namePrompt = prompt("What your name?", name ? name : "");
  name = namePrompt || "";

  return namePrompt;
}

function askAge() {
  const agePrompt = prompt("How old are you?", age ? age : "");
  age = agePrompt || "";

  validationAge();
}
