//1.
// метод, який ми викликаємо в масиві.
// в методі forEach ми передаємо функцію callback - в якості аргумента, 
// який передаємо в іншу функцію або метод.
// метод forEach дозволяє запускати функцію для кожного елемента масиву,
// поки не будуть оброблені всі елементи
//2.
// array.length = 0;
// array.splice(0, array.length);
// array = [];


//3.
// console.log(Array.isArray (arr));





const array = ["hello", "world", 23, "23", null];

const filterBy = (arr, type) => arr.filter((el) => typeof el !== type);

console.log(filterBy(array, "string"));
