
///1. 
// допомогає правильно інтерпретувати рядок.
// коли ми вводимо ім`я з апострофом, 
// то це може змінити текст, видаючи одне слово,
// як два різних. тоді ми можем скористатись зворотнім слешем,
// поставивши його перед апострофом.
// 2.
// function my() {}
// const my = function () {};
// const my = () => {};
//3. 
// це процес, у якому інтерпретатор переміщує оголошення функцій,
// змінних або класів у верхню частину їхньої області
// видимості перед виконанням коду.
// Можливість використовувати значення змінної в її області видимості
//  до рядка її оголошення. («Value hoisting»).
//Можливість посилатися на змінну в її області видимості до рядка, 
//в якому вона оголошена, без викиду помилки ReferenceError, 
//але значення завжди залишається невизначеним. 
//Оголошення змінної викликає зміни поведінки в її області видимості до рядка,
// у якому вона оголошена.



function createNewUser() {
  const firstName = prompt("Введіть ім'я");
  const lastName = prompt("Введіть прізвище:");
  const birthday = prompt("dd.mm.yyyy");

  const newUser = {
    firstName: firstName,
    lastName: lastName,
    birthday: birthday,

    getLogin: function () {
      const loginFirstName = this.firstName.charAt(0).toLowerCase();
      const loginLastName = this.lastName.toLowerCase();
      return loginFirstName + loginLastName;
    },
    setFirstName: function (name) {
      this.firstName = name;
    },
    setLastName: function (name) {
      this.lastName = name;
    },
    getAge: function () {
      // дістає поточну дату
      const today = new Date();
      const currentYear = today.getFullYear();

      // дістає дату від користувача.
      const birthdayArray = this.birthday.split(".");
      const today2 = new Date(
        birthdayArray[2],
        birthdayArray[1] - 1,
        birthdayArray[0]
      );
      console.log("today2: ", today2);
      const currentYear2 = today2.getFullYear();

      //повертає різницю між датами
      return currentYear - currentYear2;
    },
    getPassword: function () {
      const birthdayArray = this.birthday.split(".");

      const loginFirstName = this.firstName.charAt(0).toUpperCase();
      const loginLastName = this.lastName.toLowerCase();

      return loginFirstName + loginLastName + birthdayArray[2];
    },
  };

  return newUser;
}

const user = createNewUser();
const login = user.getLogin();

const password = user.getPassword();
const age = user.getAge();
console.log(user);
console.log(age);
console.log(password);


