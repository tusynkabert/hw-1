const body = document.querySelector('body');
const button = document.querySelector('#themeToggle');

const resLocal = localStorage.getItem('theme');



const getSavedTheme = localStorage.getItem('theme') ?? 'dark-theme';


if (getSavedTheme) {
    body.classList.add(getSavedTheme)
}

button.addEventListener('click', () => {
    body.classList.toggle('dark-theme')

    if (body.classList.contains('dark-theme')) {
        localStorage.setItem('theme', 'dark-theme');
    } else {
        localStorage.setItem('theme', 'light-theme');
    }
})