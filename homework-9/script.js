//1.
//html:
//<div class = "target"
//<p>text</p>
//JS:
//const targetEl = document.querySelector('.target');
//targetEl.insertAdjacentHTML("beforebegin", `<p>text1</p>`)

//2.
//elem.insertAdjacentHTML(where, html).
//"beforebegin" – вставити html безпосередньо перед elem,(сусідній зверху)
//"afterbegin" – вставити html в elem, на початку,
//"beforeend" – вставити html в elem, в кінці,
//"afterend" – вставити html безпосередньо після elem.(сусідній знизу)

//3.
//"remove"
//document.querySelector(".item").remove()

// отримуємо посилання на елементи списків та body
const list1 = document.querySelector('#list1')
const list2 = document.querySelector('#list2')
const body = document.querySelector('body');

// отримуємо посилання на елемент для показу таймера
const timerElement = document.getElementById('timer');

// Функція для рекурсивного створення списків з вмістом зазначеного масиву
const insertListElements = (array, parentEl = body) => {

    const ul = document.createElement('ul');

    array.forEach((el) => {

        const li = document.createElement(Array.isArray(el) ? 'div' : 'li');

        if (Array.isArray(el)) {
            insertListElements(el, li) // Рекурсивно створюємо дочірні списки
        } else {
            li.textContent = el;
        }

        ul.insertAdjacentHTML('beforeend', li.outerHTML)
    })
    //вставляємо список всередину батьківського елемента
    parentEl.insertAdjacentElement('afterbegin', ul)
}
// функція insertListElements для створення списків з вказаним вмістом
insertListElements(["Kharkiv", "Kyiv", ["Borispol", "Irpin"], "Odessa", "Lviv", "Dnieper"])

// функція для очищення сторінки після таймера
const clearPage = () => {
    body.innerHTML = ''; // очищаємо вміст сторінки
};
let countdown = 3;
timerElement.textContent = countdown;
// Таймер зворотнього відліку
const timerInterval = setInterval(() => {
    countdown--;
    if (countdown === 0) {
        clearInterval(timerInterval);// упиняємо інтервал таймера
        clearPage();// очищаємо сторінку
    } else {
        timerElement.textContent = countdown;// Оновлюємо вміст таймера
    }
}, 1000); 