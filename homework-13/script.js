
// 1. Опишіть своїми словами різницю між функціями `setTimeout()` і `setInterval()`.
//    //`setTimeout()`  запускається один раз.
//    //`setInterval()` запускається безкінечне виконання коду.

// 2. Що станеться, якщо в функцію `setTimeout()` передати нульову затримку? Чи спрацює вона миттєво і чому?
//    // не миттєво, але буде виконана після основного коду. Нульова затримка вказує, що функція буде поставлена в
//    // чергу для виконання якнайшвидше після завершення поточної операції.

// 3. Чому важливо не забувати викликати функцію `clearInterval()`, коли раніше створений цикл запуску вам вже не потрібен?
//    //щоб не запускати безкінечного виконання коду.

// Вибір всіх елементів img на сторінці
const images = document.querySelectorAll('img');
// Вибір кнопок "Припинити" та "Відновити показ"
const stop = document.querySelector('.stop');
const resume = document.querySelector('.resume');
// Індекс поточного зображення
let currentIndex = 0;
// Змінна для збереження ідентифікатора інтервалу
let myIntervalId;
// Функція для налаштування і запуску інтервалу
function setMyInterval() {
    myIntervalId = setInterval(() => {
        // Перебираємо всі зображення
        images.forEach((el, key) => {

            const classEl = el.classList;
            // Ховаємо всі зображення
            classEl.add('hide');
            // Показуємо поточне зображення
            if (key === currentIndex) {

                classEl.remove('hide');

            }
        });
        // Перехід до наступного зображення
        currentIndex = currentIndex + 1;
        // Повернення до першого зображення, якщо досягнуто кінець колекції
        if (currentIndex > (images.length - 1)) {
            currentIndex = 0;
        }

    }, 3000); //Інтервал в 3 секунди
}
// Перший запуск інтервалу
setMyInterval();
// Відновлення інтервалу після зупинки по кліку на кнопку "Відновити показ"
resume.addEventListener('click', setMyInterval)
// Зупинка інтервалу по кліку на кнопку "Припинити"
stop.addEventListener('click', () => {

    clearInterval(myIntervalId);

})

